/**
 * Author: Trung Hoang
 * Date: 2 February 2016
 * Description: Elevator entity class. Contains methods for creation, movement, and game events
 */

package com.tqhoang.elevator.entity;

import com.badlogic.gdx.graphics.Texture;

public class Elevator extends Entity {

    public Elevator(Texture texture) {
        super(texture);
    }
}