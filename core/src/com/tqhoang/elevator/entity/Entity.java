/**
 * Author: Trung Hoang
 * Date: 2 February 2016
 * Description: Super class containing inherited methods for other entities
 */

package com.tqhoang.elevator.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Entity {
    protected Texture texture;
    protected Rectangle object;

    public Entity(Texture texture) {
        this.texture = texture;
        object = new Rectangle();
        object.width = texture.getWidth();
        object.height = texture.getHeight();

        object.setX(200);
        object.setY(40);
    }

    public void move(int x, int y) {
        object.x += x;
        object.y += y;
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.draw(texture, object.x, object.y);
    }
}
