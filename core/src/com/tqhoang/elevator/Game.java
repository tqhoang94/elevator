/**
 * Author: Trung Hoang
 * Date: 2 February 2016
 * Description: The main class of the game
 */

package com.tqhoang.elevator;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.tqhoang.elevator.entity.Elevator;

public class Game extends ApplicationAdapter {
	private SpriteBatch spriteBatch;
	private OrthographicCamera camera;

	private Elevator elevator;
	
	@Override
	public void create () {
		spriteBatch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 240, 400);

		elevator = new Elevator(new Texture(Gdx.files.internal("badlogic.png")));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		spriteBatch.begin();
		elevator.draw(spriteBatch);
		spriteBatch.end();

		userInput();
	}

	private void userInput() {
		if (Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);

			if (touchPos.y >= 200) {
				elevator.move(0, 3);
			}
			else {
				elevator.move(0, -3);
			}
		}
	}
}
